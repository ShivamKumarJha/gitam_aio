package com.shivamkumarjha.gitamaio;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.daimajia.numberprogressbar.OnProgressBarListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , OnProgressBarListener {

    // Variables declaration
    private NumberProgressBar bnp;
    private WebView mWebView;
    private SwipeRefreshLayout swipeLayout;
    private SharedPreferences settings ;
    private SharedPreferences.Editor editor ;
    private final int REQUEST_WRITE_STORAGE = 112;

    // function to initialize variables
    public void init() {
        bnp = (NumberProgressBar)findViewById(R.id.pB1);
        mWebView = (WebView) this.findViewById(R.id.webView);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // do not tint icon colors
        navigationView.setItemIconTintList(null);

        //call gitam home page initially by default
        common_to_all("http://www.gitam.edu/");

        // now open the drawer to make sure user see's other websites are available to browse
        drawer.openDrawer(GravityCompat.START);

        //ask for the permission android 6.0 + style
        int permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("PermissionDemo", "Permission to record denied");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Permission to access the SD-CARD is required for this app to download documents.")
                        .setTitle("Permission required");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("PermissionDemo", "Clicked");
                        makeRequest();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                makeRequest();
            }
        }
    }

    public void update_check() {
        try {
            //Get installed app version name
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version1 = pInfo.versionName;
            int result1 = Integer.parseInt(version1);
            new JsonTask().execute("https://bitbucket.org/ShivamKumarJha/gitam_aio/raw/master/app/json/gaio.json");
            settings = getSharedPreferences("settings", 0);
            int result2 = settings.getInt("newversion", 123);
            final String update_url = settings.getString("newurl","");
            if (result2 > result1) {
                new AlertDialog.Builder(this)
                        .setTitle("UPDATE?")
                        .setMessage("A new update is available. Update now?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String PATH = "/mnt/sdcard/Download/";
                                File file = new File(PATH);
                                file.mkdirs();
                                File outputFile = new File(file, "app-release.apk");
                                if(outputFile.exists()){
                                    outputFile.delete();
                                }
                                common_to_all(update_url);
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    // Method to print Web page
    public  void createWebPagePrint(WebView webView) {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();
        String jobName = webView.getTitle();
        PrintAttributes.Builder builder = new PrintAttributes.Builder();
        builder.setMediaSize(PrintAttributes.MediaSize.ISO_A5);
        assert printManager != null;
        PrintJob printJob = printManager.print(jobName, printAdapter, builder.build());

        if(printJob.isCompleted()){
            Toast.makeText(getApplicationContext(), "Print completed.", Toast.LENGTH_LONG).show();
        }
        else if(printJob.isFailed()){
            Toast.makeText(getApplicationContext(), "Print failed.", Toast.LENGTH_LONG).show();
        }
        // Save the job object for later status checking
    }

    //Method to detect Internet availability
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    void makeRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_WRITE_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {

                if (grantResults.length == 0
                        || grantResults[0] !=
                        PackageManager.PERMISSION_GRANTED) {

                    Log.i("PermissionDemo", "Permission has been denied by user");

                } else {

                    Log.i("PermissionDemo", "Permission has been granted by user");

                    //This will allow the function to run in 1st ever run
                    if (isNetworkAvailable()) {
                        update_check();
                    }
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // close drawer when webview can go back
        if (drawer.isDrawerOpen(GravityCompat.START ) && mWebView.canGoBack()) {
            drawer.closeDrawer(GravityCompat.START);
        }
        // go to previous webpage
        else if(mWebView.canGoBack()){
            mWebView.goBack();
        }
        //close app when the drawer is open & view can't go back
        else if (drawer.isDrawerOpen(GravityCompat.START) && !mWebView.canGoBack()) {
            exit_app();
        }
        // for anything else open drawer
        else {
            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        init();

        // shared preferences for swiper menu
        settings = getSharedPreferences("settings", 0);
        boolean isChecked = settings.getBoolean("swiper_checkbox", true);
        MenuItem item = menu.findItem(R.id.swipe_menu);
        item.setChecked(isChecked);
        item.setChecked(isChecked);
        // Now decide to show swiper or not
        if (isChecked ) {
            swipeLayout.setEnabled( true );
        }
        else {
            swipeLayout.setEnabled( false );
        }

        isChecked = settings.getBoolean("auto_update", true);
        item = menu.findItem(R.id.autoupdate);
        item.setChecked(isChecked);
        // Now decide to auto update or not
        if (isChecked ) {
            if (isNetworkAvailable()) {
                update_check();
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        init();

        // required for proper working of checkable menu item's
        if (item.isChecked()) {
            item.setChecked(false);
        }
        else {
            item.setChecked(true);
        }

        settings = getSharedPreferences("settings", 0);
        editor = settings.edit();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about_settings) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog);
            alert.setTitle("GITAM AIO");
            alert.setMessage("\nGITAM AIO is aimed to provide one click access to useful GITAM Websites.\n\n" +
                    "Author: Shivam Kumar Jha\n\n" +
                    "E-Mail: jha.shivam3@gmail.com\n\n" +
                    "Author is a GITAMite, Hyderabad.\n\n" +
                    "Cheers!");
            final TextView input = new TextView (this);
            alert.setView(input);
            alert.show();
            return true;
        }
        else if (id == R.id.reload_menu) {
            mWebView.reload();
            return true;
        }
        else if (id == R.id.forward_menu) {
            mWebView.goForward();
            return true;
        }
        else if (id == R.id.swipe_menu) {
            if (item.isChecked()) {
                swipeLayout.setEnabled( true );
            }
            else {
                swipeLayout.setEnabled( false );
            }
            editor.putBoolean("swiper_checkbox", item.isChecked());
            editor.apply();
            return true;
        }
        else if (id == R.id.autoupdate) {
            editor.putBoolean("auto_update", item.isChecked());
            editor.apply();
            //This will allow the function to run after its checked in menu
            if (isNetworkAvailable() && item.isChecked()) {
                update_check();
            }
            return true;
        }
        else if (id == R.id.print) {
            createWebPagePrint(mWebView);
        }
        else if (id == R.id.changelog) {
            common_to_all("https://bitbucket.org/ShivamKumarJha/gitam_aio/commits/branch/master");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        init();

        if (id == R.id.gitam_website) {
            common_to_all("http://www.gitam.edu/");
        } else if (id == R.id.exam_website) {
            common_to_all("https://eweb.gitam.edu/mobile/Pages/NewGrdcrdInput1.aspx");
        } else if (id == R.id.portal_website) {
            common_to_all("https://login.gitam.edu/sign/studentlogin.aspx?id=STUDENT");
        } else if (id == R.id.xlearn_website) {
            common_to_all("http://xlearn.gitam.edu/moodle/login/index.php");
        } else if (id == R.id.gitamhub_website) {
            common_to_all("https://gitamhub.blogspot.in/");
        }
        else if (id == R.id.exit_drawer) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void exit_app() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.warning_icon)
                .setTitle("Exit?")
                .setMessage("Are you sure you want to exit GITAM AIO?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    public void mytoast (String toastmsg) {
        final Toast toast = Toast.makeText(getApplicationContext(), toastmsg, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 600);
    }

    public void common_to_all(String myurl) {
        init();

        // swipe layout configuration
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mWebView.reload();
                mytoast("Refreshing");
            }
        });
        swipeLayout.setColorSchemeResources(R.color.blue, R.color.orange, R.color.green);
        //dont allow refresh while refreshing already
        if(swipeLayout.isRefreshing()) {
            swipeLayout.setRefreshing(false);
        }

        // webview config
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.getSettings().setDisplayZoomControls(true);
        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setUseWideViewPort(true);
        mWebView.goBackOrForward(10);

        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                // show progressbar when required
                if(progress < 100 && bnp.getVisibility() == ProgressBar.GONE){
                    bnp.setVisibility(ProgressBar.VISIBLE);
                }
                bnp.setProgress(progress);
                if(progress == 100) {
                    bnp.setVisibility(ProgressBar.GONE);
                    swipeLayout.setRefreshing(false);
                }
            }
        });

        //Downloading a file directly using android download manager
        mWebView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

                request.setMimeType(mimeType);
                //------------------------COOKIE!!------------------------
                String cookies = CookieManager.getInstance().getCookie(url);
                request.addRequestHeader("cookie", cookies);
                //------------------------COOKIE!!------------------------
                request.addRequestHeader("User-Agent", userAgent);
                request.setDescription("Downloading file...");
                request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
                DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                assert dm != null;
                dm.enqueue(request);
                Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();
            }
        });

        // load url
        mWebView.loadUrl(myurl);
    }

    @Override
    public void onProgressChange(int current, int max) {

    }

    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                    Log.d("Response: ", "> " + line);   //here u'll get whole json response in android monitor
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            JSONObject update= null;
            SharedPreferences settings ;
            SharedPreferences.Editor editor ;
            settings = getSharedPreferences("settings", 0);
            editor = settings.edit();
            try {
                update = (new JSONObject(result)).getJSONObject("GAIO");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                String newurl=update.getString("url");
                editor.putString("newurl", newurl);
                editor.apply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                int version=update.getInt("version");
                editor.putInt("newversion", version);
                editor.apply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
