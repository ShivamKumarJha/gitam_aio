GITAM AIO (All In One)
One click access to all the usefull GITAM websites!
  - GITAM Home Page
  - GITAM Examination Results
  - Student portal
  - X-Learn

App features:
  - Auto Update
  - Drawer design for navigation.
  - Download manager to download files.
  - Multi Window support in Nougat & above Android versions.
  - Swipe down to refresh web page.

The app has been thoroughly tested and does not contains any bug what so ever. 
However if you find one, do let me know.
